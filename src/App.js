import './App.css';
import Main from './component/Main';
import Navbar from './component/Navbar';
import Card from './component/Card';
import obj from './Object';

function App() {
 return (
    <>
    <Navbar/>
    <Main/>
    {obj.map((item)=>{return(<> <Card obj={item}/></>);})}
    </>
  )
}

export default App;
