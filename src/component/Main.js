import React from 'react'

function Main() {
  return (
    <div className='main'>
      <img alt='picure collage' src="./images/Grouppicture.png" className='heroLogo'/>
      <h1 className='mainHeader'>Online Experiences</h1>
      <p className='mainParagraph'>Joinn unique interactive activities led by oe-of-a-kind hosts all without leaving home</p>
    </div>
  )
}

export default Main;
