import React from 'react'

function Navbar() {
  return (
    <div className='navbarLogo'>
        <img className='logoImg' src='./images/Vector.png' />
    </div>
  )
}

export default Navbar
