import React from 'react'

function Card(props) {
  return(
  <div className='card'>
    {props.obj.badgeText && <div className="cardBadge">{props.obj.badgeText}</div>}
    <img alt="card image" src={props.obj.img} className='cardimage' />
      <div className='cardDetail'>
      <img className='star' src='./images/star.png'/>
      <span className='cardText'>{props.obj.rating}</span>
      <span className='cardText'>({props.obj.reviewCount})</span>
      <span className='boldText cardText' >{props.obj.country}</span>
    </div>
    <p className='cardText heightReduceCardText'>{props.obj.title}</p>
    <p className='cardText heightReduce'><span className="bold">From {props.obj.price}</span> / person</p>     
  </div>
)
}
export default Card; 

