const obj =[
    {
    img:"./images/celebrity.png",
    rating:"5.0",
    reviewCount:6,
    country:"USA",
    title:"Life Lessons with Katie Zaferes",
    price:136,
    badgeText:"SOLD OUT"
    },
    {
    img:"./images/wedding-photography.png",
    rating:"5.0",
    reviewCount:6,
    country:"USA",
    title:"Life Lessons with Katie Zaferes",
    price:136,
    badgeText:"ONLINE"
    },
    {
    img:"./images/mountain-bike.png",
    rating:"5.0",
    reviewCount:6,
    country:"USA",
    title:"Life Lessons with Katie Zaferes",
    price:136,
    badgeText:""
    },
]
export default obj;